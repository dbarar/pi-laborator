// OpenCVApplication.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "common.h"
#include <random>
#include <algorithm>
#include <iterator>
#include <stdio.h>
std::default_random_engine gen;
std::uniform_int_distribution<int> d(0, 255);


void testOpenImage()
{
	char fname[MAX_PATH];
	while(openFileDlg(fname))
	{
		Mat src;
		src = imread(fname);
		imshow("image",src);
		waitKey();
	}
}

void testOpenImagesFld()
{
	char folderName[MAX_PATH];
	if (openFolderDlg(folderName)==0)
		return;
	char fname[MAX_PATH];
	FileGetter fg(folderName,"bmp");
	while(fg.getNextAbsFile(fname))
	{
		Mat src;
		src = imread(fname);
		imshow(fg.getFoundFileName(),src);
		if (waitKey()==27) //ESC pressed
			break;
	}
}

void testImageOpenAndSave()
{
	Mat src, dst;

	src = imread("Images/Lena_24bits.bmp", CV_LOAD_IMAGE_COLOR);	// Read the image

	if (!src.data)	// Check for invalid input
	{
		printf("Could not open or find the image\n");
		return;
	}

	// Get the image resolution
	Size src_size = Size(src.cols, src.rows);

	// Display window
	const char* WIN_SRC = "Src"; //window for the source image
	namedWindow(WIN_SRC, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_SRC, 0, 0);

	const char* WIN_DST = "Dst"; //window for the destination (processed) image
	namedWindow(WIN_DST, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_DST, src_size.width + 10, 0);

	cvtColor(src, dst, CV_BGR2GRAY); //converts the source image to a grayscale one

	imwrite("Images/Lena_24bits_gray.bmp", dst); //writes the destination to file

	imshow(WIN_SRC, src);
	imshow(WIN_DST, dst);

	printf("Press any key to continue ...\n");
	waitKey(0);
}

void testNegativeImage()
{
	char fname[MAX_PATH];
	while(openFileDlg(fname))
	{
		double t = (double)getTickCount(); // Get the current time [s]
		
		Mat src = imread(fname,CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat dst = Mat(height,width,CV_8UC1);
		// Asa se acceseaaza pixelii individuali pt. o imagine cu 8 biti/pixel
		// Varianta ineficienta (lenta)
		for (int i=0; i<height; i++)
		{
			for (int j=0; j<width; j++)
			{
				uchar val = src.at<uchar>(i,j);
				uchar neg = 255 - val;
				dst.at<uchar>(i,j) = neg;
			}
		}

		// Get the current time again and compute the time difference [s]
		t = ((double)getTickCount() - t) / getTickFrequency();
		// Print (in the console window) the processing time in [ms] 
		printf("Time = %.3f [ms]\n", t * 1000);

		imshow("input image",src);
		imshow("negative image",dst);
		waitKey();
	}
}

void testParcurgereSimplaDiblookStyle()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat dst = src.clone();

		double t = (double)getTickCount(); // Get the current time [s]

		// the fastest approach using the �diblook style�
		uchar *lpSrc = src.data;
		uchar *lpDst = dst.data;
		int w = (int) src.step; // no dword alignment is done !!!
		for (int i = 0; i<height; i++)
			for (int j = 0; j < width; j++) {
				uchar val = lpSrc[i*w + j];
				lpDst[i*w + j] = 255 - val;
			}

		// Get the current time again and compute the time difference [s]
		t = ((double)getTickCount() - t) / getTickFrequency();
		// Print (in the console window) the processing time in [ms] 
		printf("Time = %.3f [ms]\n", t * 1000);

		imshow("input image",src);
		imshow("negative image",dst);
		waitKey();
	}
}

void testColor2Gray()
{
	char fname[MAX_PATH];
	while(openFileDlg(fname))
	{
		Mat src = imread(fname);

		int height = src.rows;
		int width = src.cols;

		Mat dst = Mat(height,width,CV_8UC1);

		// Asa se acceseaaza pixelii individuali pt. o imagine RGB 24 biti/pixel
		// Varianta ineficienta (lenta)
		for (int i=0; i<height; i++)
		{
			for (int j=0; j<width; j++)
			{
				Vec3b v3 = src.at<Vec3b>(i,j);
				uchar b = v3[0];
				uchar g = v3[1];
				uchar r = v3[2];
				dst.at<uchar>(i,j) = (r+g+b)/3;
			}
		}
		
		imshow("input image",src);
		imshow("gray image",dst);
		waitKey();
	}
}

void testBGR2HSV()
{
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname);
		int height = src.rows;
		int width = src.cols;

		// Componentele d eculoare ale modelului HSV
		Mat H = Mat(height, width, CV_8UC1);
		Mat S = Mat(height, width, CV_8UC1);
		Mat V = Mat(height, width, CV_8UC1);

		// definire pointeri la matricele (8 biti/pixeli) folosite la afisarea componentelor individuale H,S,V
		uchar* lpH = H.data;
		uchar* lpS = S.data;
		uchar* lpV = V.data;

		Mat hsvImg;
		cvtColor(src, hsvImg, CV_BGR2HSV);

		// definire pointer la matricea (24 biti/pixeli) a imaginii HSV
		uchar* hsvDataPtr = hsvImg.data;

		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				int hi = i*width * 3 + j * 3;
				int gi = i*width + j;

				lpH[gi] = hsvDataPtr[hi] * 510 / 360;		// lpH = 0 .. 255
				lpS[gi] = hsvDataPtr[hi + 1];			// lpS = 0 .. 255
				lpV[gi] = hsvDataPtr[hi + 2];			// lpV = 0 .. 255
			}
		}

		imshow("input image", src);
		imshow("H", H);
		imshow("S", S);
		imshow("V", V);

		waitKey();
	}
}

void testResize()
{
	char fname[MAX_PATH];
	while(openFileDlg(fname))
	{
		Mat src;
		src = imread(fname);
		Mat dst1,dst2;
		//without interpolation
		resizeImg(src,dst1,320,false);
		//with interpolation
		resizeImg(src,dst2,320,true);
		imshow("input image",src);
		imshow("resized image (without interpolation)",dst1);
		imshow("resized image (with interpolation)",dst2);
		waitKey();
	}
}

void testCanny()
{
	char fname[MAX_PATH];
	while(openFileDlg(fname))
	{
		Mat src,dst,gauss;
		src = imread(fname,CV_LOAD_IMAGE_GRAYSCALE);
		double k = 0.4;
		int pH = 50;
		int pL = (int) k*pH;
		GaussianBlur(src, gauss, Size(5, 5), 0.8, 0.8);
		Canny(gauss,dst,pL,pH,3);
		imshow("input image",src);
		imshow("canny",dst);
		waitKey();
	}
}

void testVideoSequence()
{
	VideoCapture cap("Videos/rubic.avi"); // off-line video from file
	//VideoCapture cap(0);	// live video from web cam
	if (!cap.isOpened()) {
		printf("Cannot open video capture device.\n");
		waitKey(0);
		return;
	}
		
	Mat edges;
	Mat frame;
	char c;

	while (cap.read(frame))
	{
		Mat grayFrame;
		cvtColor(frame, grayFrame, CV_BGR2GRAY);
		Canny(grayFrame,edges,40,100,3);
		imshow("source", frame);
		imshow("gray", grayFrame);
		imshow("edges", edges);
		c = cvWaitKey(0);  // waits a key press to advance to the next frame
		if (c == 27) {
			// press ESC to exit
			printf("ESC pressed - capture finished\n"); 
			break;  //ESC pressed
		};
	}
}


void testSnap()
{
	VideoCapture cap(0); // open the deafult camera (i.e. the built in web cam)
	if (!cap.isOpened()) // openenig the video device failed
	{
		printf("Cannot open video capture device.\n");
		return;
	}

	Mat frame;
	char numberStr[256];
	char fileName[256];
	
	// video resolution
	Size capS = Size((int)cap.get(CV_CAP_PROP_FRAME_WIDTH),
		(int)cap.get(CV_CAP_PROP_FRAME_HEIGHT));

	// Display window
	const char* WIN_SRC = "Src"; //window for the source frame
	namedWindow(WIN_SRC, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_SRC, 0, 0);

	const char* WIN_DST = "Snapped"; //window for showing the snapped frame
	namedWindow(WIN_DST, CV_WINDOW_AUTOSIZE);
	cvMoveWindow(WIN_DST, capS.width + 10, 0);

	char c;
	int frameNum = -1;
	int frameCount = 0;

	for (;;)
	{
		cap >> frame; // get a new frame from camera
		if (frame.empty())
		{
			printf("End of the video file\n");
			break;
		}

		++frameNum;
		
		imshow(WIN_SRC, frame);

		c = cvWaitKey(10);  // waits a key press to advance to the next frame
		if (c == 27) {
			// press ESC to exit
			printf("ESC pressed - capture finished");
			break;  //ESC pressed
		}
		if (c == 115){ //'s' pressed - snapp the image to a file
			frameCount++;
			fileName[0] = NULL;
			sprintf(numberStr, "%d", frameCount);
			strcat(fileName, "Images/A");
			strcat(fileName, numberStr);
			strcat(fileName, ".bmp");
			bool bSuccess = imwrite(fileName, frame);
			if (!bSuccess) 
			{
				printf("Error writing the snapped image\n");
			}
			else
				imshow(WIN_DST, frame);
		}
	}

}

void get_aria(Mat *src, int x, int y, int* p_aria) {
	Vec3b color = src->at<Vec3b>(y, x);
	int aria = 0;
	for (int i = 0; i < src->rows; i++)
		for (int j = 0; j < src->cols; j++)
			if (color == src->at<Vec3b>(i, j))
				aria++;
	*p_aria = aria;
	printf("Aria: %d\n", aria);
}

void get_centru_masa(Mat *src, int x, int y, int *col, int *row) {
	Vec3b color = src->at<Vec3b>(y, x);
	Mat centru_img = (*src).clone();

	int abscise = 0;
	int ordonate = 0;
	int aria = 0;

	for (int i = 0; i < src->rows; i++)
		for (int j = 0; j < src->cols; j++)
			if (color == src->at<Vec3b>(i, j)) {
				aria++;
				abscise += j;
				ordonate += i;
			}
	*col = abscise / aria;
	*row = ordonate / aria;
	printf("Centru de masa: %d x %d y\n", col, row);

	centru_img.at<Vec3b>(*row, *col) = Vec3b(0, 0, 0);
	imshow("centru masa", centru_img);
}

void get_axa_de_alungire(Mat* src, int x, int y, int r, int c) {
	Vec3b color = src->at<Vec3b>(y, x);

	float X = 0.0f;
	float Y;
	float Y1 = 0.0f;
	float Y2 = 0.0f;
	float fi;
	int r_max = 0;
	int r_min = src->rows;
	int c_max = 0;
	int c_min = src->cols;
	for (int i = 0; i < src->rows; i++)
		for (int j = 0; j < src->cols; j++)
			if (color == src->at<Vec3b>(i, j)) {
				X += (i - r) * (j - c);
				Y1 += (j - c) * (j - c);
				Y2 += (i - r) * (i - r);
				r_max = i > r_max ? i : r_max;
				r_min = i < r_min ? i : r_min;
				c_max = j > c_max ? j : c_max;
				c_min = j < c_min ? j : c_min;
			}
	X = 2 * X;
	Y = Y1 - Y2;
	fi = 0.5 * atan2(X, Y);
	if (fi < 0)
		fi += CV_PI;

	//trasare axa
	float ra = r + tan(fi) * (c_min - c);
	float rb = r + tan(fi) * (c_max - c);

	Mat dst = (*src).clone();
	line(dst, Point(c_min, ra), Point(c_max, rb), 0);

	imshow("axa image", dst);
	//din radian
	fi = fi * 180 / CV_PI;
	printf("Unghiul: %f", fi);
}

bool vectori_vecini(Mat *src, int row, int col, Vec3b color)
{
	return color != src->at<Vec3b>(row - 1, col - 1) ||
		color != src->at<Vec3b>(row - 1, col) ||
		color != src->at<Vec3b>(row - 1, col + 1) ||
		color != src->at<Vec3b>(row, col + 1) ||
		color != src->at<Vec3b>(row + 1, col + 1) ||
		color != src->at<Vec3b>(row + 1, col) ||
		color != src->at<Vec3b>(row + 1, col - 1) ||
		color != src->at<Vec3b>(row, col - 1);
}
void get_perimetru(Mat *src, int x, int y, int *p_perim) {
	Vec3b color = src->at<Vec3b>(y, x);
	Mat contur(src->rows, src->cols, CV_8UC3, Scalar(255, 255, 255));
	int perimetru = 0;
	for (int i = 0; i < src->rows; i++)
		for (int j = 0; j < src->cols; j++)
			if (color == src->at<Vec3b>(i, j)) {
				if (vectori_vecini(src, i, j, color)) {
					perimetru++;
					contur.at<Vec3b>(i, j) = color;
				}
			}
	perimetru = perimetru * CV_PI / 4;
	*p_perim = perimetru;
	printf("Perimetru: %d\n", perimetru);

	imshow("Contur", contur);
}

void get_factor_de_subtiere(int aria, int perim, float *p_fact) {
	float fact;
	fact = 4.0 * CV_PI * aria / (perim * perim);
	*p_fact = fact;

	printf("Factorul de subtiere: %f\n", fact);
}

void get_elongatie(Mat *src, int x, int y, float *p_R) {
	Vec3b color = src->at<Vec3b>(y, x);

	int r_max = 0;
	int r_min = src->rows;
	int c_max = 0;
	int c_min = src->cols;
	float R;

	for (int i = 0; i < src->rows; i++)
		for (int j = 0; j < src->cols; j++)
			if (color == src->at<Vec3b>(i, j)) {
				r_max = i > r_max ? i : r_max;
				r_min = i < r_min ? i : r_min;
				c_max = j > c_max ? j : c_max;
				c_min = j < c_min ? j : c_min;
			}

	R = (float)(c_max - c_min + 1) / (r_max - r_min + 1);
	*p_R = R;
	printf("Elongatia: %f\n", R);
}

void get_proiectie_orizontala(Mat *src, int x, int y) {
	Vec3b color = src->at<Vec3b>(y, x);
	Mat prOriz(src->rows, src->cols, CV_8UC3, Scalar(255, 255, 255));
	for (int i = 0; i < src->rows; i++)
		for (int k = 0, j = 0; j < src->cols; j++)
			if (color == src->at<Vec3b>(i, j))
				prOriz.at<Vec3b>(i, k++) = color;

	imshow("Proiectie orizontala ", prOriz);
}
void get_proiectie_verticala(Mat *src, int x, int y) {
	Vec3b color = src->at<Vec3b>(y, x);
	Mat prVert(src->rows, src->cols, CV_8UC3, Scalar(255, 255, 255));
	for (int j = 0; j < src->cols; j++)
		for (int k = 0, i = 0; i < src->rows; i++)
			if (color == src->at<Vec3b>(i, j))
				prVert.at<Vec3b>(k++, j) = color;

	imshow("Proiectie verticala ", prVert);
}
void MyCallBackFunc(int event, int x, int y, int flags, void* param)
{
	//More examples: http://opencvexamples.blogspot.com/2014/01/detect-mouse-clicks-and-moves-on-image.html
	Mat* src = (Mat*)param;
	int aria;
	int col;
	int row;
	int perim;
	float fact;
	float elongatie;
	if (event == CV_EVENT_LBUTTONDBLCLK)//CV_EVENT_LBUTTONDOWN
	{
		printf("Pos(x,y): %d,%d  Color(RGB): %d,%d,%d\n",
			x, y,
			(int)(*src).at<Vec3b>(y, x)[2],
			(int)(*src).at<Vec3b>(y, x)[1],
			(int)(*src).at<Vec3b>(y, x)[0]);
		get_aria(src, x, y, &aria);
		get_centru_masa(src, x, y, &col, &row);
		get_axa_de_alungire(src, x, y, row, col);
		get_perimetru(src, x, y, &perim);
		get_factor_de_subtiere(aria, perim, &fact);
		get_elongatie(src, x, y, &elongatie);
		get_proiectie_orizontala(src, x, y);
		get_proiectie_verticala(src, x, y);

		printf("\n");
	}
}


void testMouseClick()
{
	Mat src;
	// Read image from file 
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		src = imread(fname);
		//Create a window
		namedWindow("My Window", 1);

		//set the callback function for any mouse event
		setMouseCallback("My Window", MyCallBackFunc, &src);

		//show the image
		imshow("My Window", src);

		// Wait until user press some key
		waitKey(0);
	}
}

/* Histogram display function - display a histogram using bars (simlilar to L3 / PI)
Input:
name - destination (output) window name
hist - pointer to the vector containing the histogram values
hist_cols - no. of bins (elements) in the histogram = histogram image width
hist_height - height of the histogram image
Call example:
showHistogram ("MyHist", hist_dir, 255, 200);
*/
void showHistogram(const std::string& name, int* hist, const int  hist_cols, const int hist_height)
{
	Mat imgHist(hist_height, hist_cols, CV_8UC3, CV_RGB(255, 255, 255)); // constructs a white image

																		 //computes histogram maximum
	int max_hist = 0;
	for (int i = 0; i<hist_cols; i++)
		if (hist[i] > max_hist)
			max_hist = hist[i];
	double scale = 1.0;
	scale = (double)hist_height / max_hist;
	int baseline = hist_height - 1;

	for (int x = 0; x < hist_cols; x++) {
		Point p1 = Point(x, baseline);
		Point p2 = Point(x, baseline - cvRound(hist[x] * scale));
		line(imgHist, p1, p2, CV_RGB(255, 0, 255)); // histogram bins colored in magenta
	}

	imshow(name, imgHist);
}

void testMakeLabels() {
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		int label = 0;
		Mat labels(height, width, CV_32SC1, Scalar(0));
		Mat dst = Mat(height, width, CV_8UC3);
		
		for (int i = 1; i < height - 1; i++)
			for (int j = 1; j < width - 1; j++) 
				if (src.at<uchar>(i, j) == 0 && labels.at<int>(i, j) == 0) {
					label++;
					std::queue<Point2i> Q;
					labels.at<int>(i, j) = label;
					Q.push(Point2i(j, i));
					while (!Q.empty()) {
						Point2i q = Q.front();
						Q.pop();

						for(int k = q.y - 1; k <= q.y + 1; k++)
							for (int l = q.x - 1; l <= q.x + 1; l++) {
								if(k < height && l < width && k > 0 && l > 0)
									if (src.at<uchar>(k, l) == 0 && labels.at<int>(k, l) == 0) {
										labels.at<int>(k, l) = label;
										Q.push(Point2i(l, k));
									}
							}
					}
				}
		
		Vec3b *colors = new Vec3b[label + 1];

		for (int i = 0; i < label + 1; i++) {
			colors[i] = Vec3b(d(gen), d(gen), d(gen));
		}
		
		for (int i = 1; i < height - 1; i++)
			for (int j = 1; j < width - 1; j++)
				dst.at<Vec3b>(i, j) = colors[labels.at<int>(i, j)];

		imshow("dst", dst);
		waitKey(0);
	}
}

void testMakeLabels2() {
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		int label = 0;
		Mat labels(height, width, CV_32SC1, Scalar(0));
		Mat dst = Mat(height, width, CV_8UC3);

		std::vector<std::vector<int>> edges;

		for (int i = 1; i < height - 1; i++)
			for (int j = 1; j < width - 1; j++)
				if (src.at<uchar>(i, j) == 0 && labels.at<int>(i, j) == 0) {
					std::vector<int> L;
					//vecini
					for (int k = i - 1; k <= i; k++)
						for (int l = j - 1; l <= j + 1; l++)
							if (labels.at<int>(k, l) > 0)
								L.push_back(labels.at<int>(k, l));
					if (L.size() == 0) {
						label++;
						labels.at<int>(i, j) = label;
						edges.resize(label + 1);
					}
					else {
						int x = L[0];
						for (int i = 0; i < L.size(); i++)
							if (L[i] < x)
								x = L[i];
						labels.at<int>(i, j) = x;
						for (int i = 0; i < L.size(); i++) {
							int y = L[i];
							if (y != x) {
								edges[x].push_back(y);
								edges[y].push_back(x);
							}
						}
					}
				}

		int newLabel = 0;
		std::vector<int> newLabels(label + 1, 0);
		for (int i = 1; i < label; i++)
			if (newLabels[i] == 0) {
				newLabel++;
				std::queue<int> Q;
				newLabels[i] = newLabel;
				Q.push(i);
				while (!Q.empty()) {
					int x = Q.front();
					Q.pop();
					for (int i = 0; i < edges[x].size(); i++) {
						int y = edges[x][i];
						if (newLabels[y] == 0) {
							newLabels[y] = newLabel;
							Q.push(y);
						}
					}
				}

			}

		Vec3b *colors = new Vec3b[newLabel + 1];

		for (int i = 0; i < newLabel + 1; i++) {
			colors[i] = Vec3b(d(gen), d(gen), d(gen));
		}

		for (int i = 1; i < height - 1; i++)
			for (int j = 1; j < width - 1; j++)
				dst.at<Vec3b>(i, j) = colors[newLabels[labels.at<int>(i, j)]];

		imshow("dst", dst);
		waitKey(0);
	}
}

void testChangeGreyLevelAditivFactor()
{
	char fname[MAX_PATH];
	int aditivFactor = 0;
	printf("Aditiv Factor (between 0 - 255): ");
	scanf("%d", &aditivFactor);

	while (openFileDlg(fname))
	{
		double t = (double)getTickCount(); // Get the current time [s]

		Mat src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat dst = Mat(height, width, CV_8UC1);
		// Asa se acceseaaza pixelii individuali pt. o imagine cu 8 biti/pixel
		// Varianta ineficienta (lenta)
		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				uchar val = src.at<uchar>(i, j);
				uchar greyLevel = val + aditivFactor;
				if (greyLevel > 255)
					greyLevel = 255;
				if (greyLevel < 0)
					greyLevel = 0;
				dst.at<uchar>(i, j) = greyLevel;
			}
		}

		// Get the current time again and compute the time difference [s]
		t = ((double)getTickCount() - t) / getTickFrequency();
		// Print (in the console window) the processing time in [ms] 
		printf("Time = %.3f [ms]\n", t * 1000);

		imshow("input image", src);
		imshow("negative image", dst);
		waitKey();
	}
}

void testChangeGreyLevelMultiplyFactor()
{
	Mat dst;
	Mat src;
	char fname[MAX_PATH];
	int multiplyLevel = 1;
	printf("Multiply Factor (~between 0 - 5): ");
	scanf("%d", &multiplyLevel);

	while (openFileDlg(fname))
	{
		double t = (double)getTickCount(); // Get the current time [s]

		src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		dst = Mat(height, width, CV_8UC1);
		// Asa se acceseaaza pixelii individuali pt. o imagine cu 8 biti/pixel
		// Varianta ineficienta (lenta)
		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				uchar val = src.at<uchar>(i, j);
				uchar greyLevel = val * multiplyLevel;
				if (greyLevel > 255)
					greyLevel = 255;
				if (greyLevel < 0)
					greyLevel = 0;
				dst.at<uchar>(i, j) = greyLevel;
			}
		}

		// Get the current time again and compute the time difference [s]
		t = ((double)getTickCount() - t) / getTickFrequency();
		// Print (in the console window) the processing time in [ms] 
		printf("Time = %.3f [ms]\n", t * 1000);

		imshow("input image", src);
		imshow("negative image", dst);

		waitKey();
	}
	//imwrite("Images/multiplyLevel.bmp", dst); //writes the destination to file
	imwrite("D:/DBarar-PI/Lab1/OpenCVApplication-VS2013_OCV2413_basic/OpenCVApplication-VS2013_OCV2413_basic/Images/multiplyLevel.bmp", dst); //writes the destination to file
}

void testColorImageWhiteRedGreenYellow()
{
	uchar x = 256;
	uchar y = 256;
	uchar middle = 128;
	Mat img(256, 256, CV_8UC3);
	//Vec3b pixel;
	for (int i = 0; i < 256; i++)
	{
		for (int j = 0; j < 256; j++)
		{
			if (i < 128 && j < 128)
				img.at<Vec3b>(i, j) = *(new Vec3b((uchar)0, (uchar)255, (uchar)0));
			if (i < 128 && j >= 128)
				img.at<Vec3b>(i, j) = *(new Vec3b((uchar)255, (uchar)255, (uchar)0));
			if (i >= 128 && j < 128)
				img.at<Vec3b>(i, j) = *(new Vec3b((uchar)0, (uchar)255, (uchar)255));
			if (i >= 128 && j >= 128)
				img.at<Vec3b>(i, j) = *(new Vec3b((uchar)0, (uchar)0, (uchar)0));
		}
	}

	imshow("output image", img);
	waitKey(0);
}

void testInverseMatrix() {
	float vals[9] = { 7, 8, 1, 2, 3, 4, 3, 4, 5 };
	Mat matrix(3, 3, CV_32FC1, vals);
	Mat inverseMatrix = matrix.inv();
	std::cout << inverseMatrix;
	getchar();
	getchar();
}
void copyRGBDifferentMatrixes() {
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname);

		int height = src.rows;
		int width = src.cols;

		Mat matrixR(height, width, CV_8UC1);
		Mat matrixG(height, width, CV_8UC1);
		Mat matrixB(height, width, CV_8UC1);

		// Asa se acceseaaza pixelii individuali pt. o imagine RGB 24 biti/pixel
		// Varianta ineficienta (lenta)
		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				Vec3b v3 = src.at<Vec3b>(i, j);
				uchar b = v3[0];
				uchar g = v3[1];
				uchar r = v3[2];
				matrixR.at<uchar>(i, j) = r;
				matrixG.at<uchar>(i, j) = g;
				matrixB.at<uchar>(i, j) = b;
			}
		}

		imshow("input image", src);
		imshow("Red matrix image", matrixR);
		imshow("Green matrix image", matrixG);
		imshow("Blue matrix image", matrixB);
		waitKey();
	}
}

void RGBtoGrayscale() {
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname);

		int height = src.rows;
		int width = src.cols;

		Mat grayScale(height, width, CV_8UC1);

		// Asa se acceseaaza pixelii individuali pt. o imagine RGB 24 biti/pixel
		// Varianta ineficienta (lenta)
		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				Vec3b v3 = src.at<Vec3b>(i, j);
				uchar b = v3[0];
				uchar g = v3[1];
				uchar r = v3[2];
				grayScale.at<uchar>(i, j) = (r + b + g) / 3;
			}
		}

		imshow("input image", src);
		imshow("Grayscale image", grayScale);
		waitKey();
	}
}

void Grayscale2WhiteBlack() {
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname);

		int height = src.rows;
		int width = src.cols;
		int threshold = 0;
		printf("Give me the threshold\n");
		scanf("%d", &threshold);

		Mat grayScale(height, width, CV_8UC1);
		Mat whiteBlack(height, width, CV_8UC1);
		// Asa se acceseaaza pixelii individuali pt. o imagine RGB 24 biti/pixel
		// Varianta ineficienta (lenta)
		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				Vec3b v3 = src.at<Vec3b>(i, j);
				uchar b = v3[0];
				uchar g = v3[1];
				uchar r = v3[2];
				int med = (r + b + g) / 3;
				grayScale.at<uchar>(i, j) = med;
				if (med < threshold)
					whiteBlack.at<uchar>(i, j) = 0;
				else
					whiteBlack.at<uchar>(i, j) = 255;
			}
		}

		imshow("input image", src);
		imshow("Grayscale image", grayScale);
		imshow("BlackWhite image", whiteBlack);
		waitKey();
	}
}

void RGBtoHSV() {
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat src = imread(fname);

		int height = src.rows;
		int width = src.cols;
		int threshold = 0;

		Mat matrixH(height, width, CV_8UC1);
		Mat matrixS(height, width, CV_8UC1);
		Mat matrixV(height, width, CV_8UC1);
		// Asa se acceseaaza pixelii individuali pt. o imagine RGB 24 biti/pixel
		// Varianta ineficienta (lenta)
		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				Vec3b v3 = src.at<Vec3b>(i, j);
				uchar B = v3[0];
				uchar G = v3[1];
				uchar R = v3[2];

				float V;
				float S;
				float H;

				float r, g, b;

				r = (float)R / 255;
				g = (float)G / 255;
				b = (float)B / 255;

				float M = max(r, max(g, b));
				float m = min(r, min(g, b));
				float C = M - m;

				V = M;

				if (C)
					S = C / V;
				else
					S = 0;

				if (C) {
					if (M == r)
						H = 60 * (g - b) / C;
					if (M == g)
						H = 120 + 60 * (b - r) / C;
					if (M == b)
						H = 240 + 60 * (r - g) / C;
				}
				else
					H = 0;
				if (H < 0)
					H = H + 360;

				matrixH.at<uchar>(i, j) = H * 255 / 360;
				matrixS.at<uchar>(i, j) = S * 255;
				matrixV.at<uchar>(i, j) = V * 255;
			}
		}

		imshow("input image", src);
		imshow("Hue image", matrixH);
		imshow("Saturation image", matrixS);
		imshow("Value image", matrixV);
		waitKey();
	}
}

void findNext(Mat_<uchar> src, Point2i pCurrent, int *dir, Point2i *pNext) {
	int dy[8] = { 0, -1, -1, -1, 0, 1, 1, 1 };
	int dx[8] = { 1, 1, 0, -1, -1, -1, 0, 1 };

	(*dir)--;
	do {		
		*dir = (*dir + 1) % 8;

		pNext->x = pCurrent.x + dx[*dir];
		pNext->y = pCurrent.y + dy[*dir];
	} while (src(pNext->y, pNext->x) != 0);
}

void getNext(Point2i* pCurrent, int dir) {
	int dy[8] = { 0, -1, -1, -1, 0, 1, 1, 1 };
	int dx[8] = { 1, 1, 0, -1, -1, -1, 0, 1 };

	pCurrent->x = pCurrent->x + dx[dir];
	pCurrent->y = pCurrent->y + dy[dir];
}

void printVector(int *vec, int size) {
	for (int i = 0; i < size; i++)
		std::cout << vec[i] << " ";
	std::cout << "\n";
}

void testContur() {
	char fname[MAX_PATH];
	while (openFileDlg(fname))
	{
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst(height, width, uchar(255));
		int dir = 7;
		int step = 0;
		Point2i p0;
		Point2i p1;
		Point2i pCurrent;
		Point2i pPrevious;
		Point2i pNext;
		int directions[4000];
		int derivates[4000];
		int iteratorDirections = 0;
		int iteratorDerivates = 0;

		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				if (src(i, j) == 0) {
					p0.x = j;
					p0.y = i;
					i = height;
					j = width;
				}

		pCurrent = p0;

		do {
			if (dir % 2 == 0)
				dir = (dir + 7) % 8;
			else
				dir = (dir + 6) % 8;

			findNext(src, pCurrent, &dir, &pNext);			

			dst(pNext.y, pNext.x) = 0;
			step++;
			if (step == 1)	p1 = pNext;

			pPrevious = pCurrent;
			pCurrent = pNext;
			directions[iteratorDirections++] = dir;
		} while (!(pCurrent.x == p1.x && pCurrent.y == p1.y && pPrevious.x == p0.x && pPrevious.y == p0.y && step > 1));

		for (int i = 0; i < iteratorDirections - 1; i++)
			derivates[iteratorDerivates++] = (directions[i + 1] - directions[i - 1] + 8) % 8;
		derivates[iteratorDerivates++] = 0;

		std::cout << "\ndirections:" << "\n";
		printVector(directions, iteratorDirections);
		std::cout << "\nderivates:" << "\n";
		printVector(derivates, iteratorDerivates);

		imshow("original image:", src);
		imshow("Final image ", dst);
		waitKey(0);
	}
}

void testConturFisier() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {

		const char *fileName = "D:\\Drive\\2017\\PI-Marita\\Lab\\OpenCVApplication-VS2017_OCV340_basic\\OpenCVApplication-VS2017_OCV340_basic\\Images\\files_border_tracing\\reconstruct.txt";
		Mat_<uchar> dst = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		FILE *f = fopen(fileName, "r");
		int *directions;
		Point2i start;
		Point2i *current;
		int directionsNumber;
		int x, y;

		if (f == NULL) {
			printf("Nu s-a putut deschide fisierul");
			return;
		}	
		fscanf(f, "%d", &(start.y));
		fscanf(f, "%d", &(start.x));
		fscanf(f, "%d", &directionsNumber);
		std::cout << start.x << " " << start.y;

		directions = (int *)malloc(directionsNumber * sizeof(int));
		if (directions == NULL) {
			printf("Nu s-a alocat");
			return;
		}
		for (int i = 0; i < directionsNumber; i++)
			fscanf(f, "%d", &(directions[i]));		
		printVector(directions, directionsNumber);
		std::cout << "\n";

		current = &start;
		
		for (int i = 0; i < directionsNumber; i++) {
			dst(current->y, current->x) = 0;
			getNext(current, directions[i]);
		}

		fclose(f);
		free(directions);
		imshow("Image: ", dst);
	}
}

Mat_<uchar> testODilatare(Mat_<uchar> src) {

	int height = src.rows;
	int width = src.cols;

	Mat_<uchar> dst(height, width, uchar(255));

	for (int i = 1; i < height - 1; i++)
		for (int j = 1; j < width - 1; j++)
			if (src(i, j) == 0) {
				dst(i, j) = 0;
				dst(i - 1, j) = 0;
				dst(i + 1, j) = 0;
				dst(i, j - 1) = 0;
				dst(i, j + 1) = 0;
			}
	return dst;
}
void testDilatare() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = src.clone();
		int n;
		scanf("%d", &n);

		for (int i = 0; i < n; i++) {
			dst = testODilatare(dst);
		}

		imshow("original image:", src);
		imshow("Final image ", dst);
		waitKey(0);
	}
}
/*
void testDilatare(){
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);		
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst(height, width, uchar(255));

		for (int i = 1; i < height - 1; i++)
			for (int j = 1; j < width - 1; j++)
				if (src(i, j) == 0) {
					dst(i, j) = 0;
					dst(i - 1, j) = 0;
					dst(i + 1, j) = 0;
					dst(i, j - 1) = 0;
					dst(i, j + 1) - 0;
				}
		imshow("original image:", src);
		imshow("Final image ", dst);
		waitKey(0);
	}
}*/
Mat_<uchar> testOEroziune(Mat_<uchar> src) {

	int height = src.rows;
	int width = src.cols;

	Mat_<uchar> dst(height, width, uchar(255));

	for (int i = 1; i < height - 1; i++)
		for (int j = 1; j < width - 1; j++)
			if (src(i, j) == 0 && src(i - 1, j) == 0 && src(i + 1, j) == 0 && src(i, j - 1) == 0 && src(i, j + 1) == 0) {
				dst(i, j) = 0;
			}
	return dst;
}
void testEroziune() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = src.clone();
		int n;
		scanf("%d", &n);

		for (int i = 0; i < n; i++) {
			dst = testOEroziune(dst);
		}

		imshow("original image:", src);
		imshow("Final image ", dst);
		waitKey(0);
	}
}
void testDeschidere() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = src.clone();

		dst = testOEroziune(src);
		dst = testODilatare(dst);

		imshow("original image:", src);
		imshow("Final image ", dst);
		waitKey(0);
	}
}
void testInchidere() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = src.clone();

		dst = testODilatare(src);
		dst = testOEroziune(dst);	

		imshow("original image:", src);
		imshow("Final image ", dst);
		waitKey(0);
	}
}
Mat_<uchar> substractImages(Mat_<uchar> a, Mat_<uchar> b) {
	int height = a.rows;
	int width = a.cols;
	Mat_<uchar> dst(height, width, uchar(255));

	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++) {
			if (a(i, j) == 0 && b(i, j) != 0) {
				dst(i, j) = 0;
			}
		}
	return dst;
}
void testExtragereContur() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = src.clone();
		Mat_<uchar> eroded = src.clone();

		eroded = testOEroziune(src);
		dst = substractImages(src, eroded);

		imshow("original image:", src);
		imshow("Final image ", dst);
		waitKey(0);
	}
}

Mat_<uchar> complementImage(Mat_<uchar> src) {
	int height = src.rows;
	int width = src.cols;
	Mat_<uchar> dst(height, width, uchar(255));

	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++) {
			if (src(i, j) == 0)
				dst(i, j) = 255;
			else
				dst(i, j) = 0;
		}
	return dst;
}

boolean compareImages(Mat_<uchar> a, Mat_<uchar> b) {
	int height = a.rows;
	int width = a.cols;
	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++) 
			if (a(i, j) != b(i, j))
				return false;

	return true;
}
Mat_<uchar> intersectImages(Mat_<uchar> a, Mat_<uchar> b) {
	int height = a.rows;
	int width = a.cols;
	Mat_<uchar> dst(height, width, uchar(255));

	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++) {
			if (a(i, j) == 0 && b(i, j) == 0) {
				dst(i, j) = 0;
			}
		}
	return dst;
}
void testUmplereRegiune() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst1(height, width, uchar(255));
		Mat_<uchar> dst(height, width, uchar(255));
		Mat_<uchar> srcComplement = complementImage(src);
		Mat_<uchar> auxForCompare(height, width, uchar(255));
		Point2i start(height / 2, width / 2);		
		
		dst1(start.y, start.x) = 0;
		//imshow("first image:", srcComplement);
		auxForCompare = dst1;

		do {			
			dst = testODilatare(dst1);				
			dst = intersectImages(dst, srcComplement);

			auxForCompare = dst1;
			dst1 = dst;
		}
		while (compareImages(auxForCompare, dst) == false);

		imshow("original image:", src);
		imshow("Final image ", dst);
		waitKey(0);
	}
}

void testHistograma() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;

		int h[256] = { 0 };
		float p[256];
		float M = height * width;
		float media = 0;
		float deviatia;
		float devSuma = 0;

		//histograma
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				h[src(i, j)] ++;
		//probabilitatea si media
		for (int i = 0; i < 256; i++) {
			p[i] = h[i] / M;
			media = media + (i * p[i]);
		}
		media = (int)media;
		
		//deviatia
		for (int i = 0; i < 256; i++) {
			devSuma = devSuma + pow((i - media), 2) * p[i];
		}
		deviatia = sqrt(devSuma);
		deviatia = (int)deviatia;

		std::cout << "\nmedia" << media;
		std::cout << "\ndeviatia" << deviatia;

		showHistogram("Histogram", h, 256, 500);
	}
}

int* histograma(Mat_<uchar> src, char *nume) {
	int height = src.rows;
	int width = src.cols;

	int *h = (int*) malloc(256*sizeof(int));

	for (int i = 0; i < 256; i++)
		h[i] = 0;

	//histograma
	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++)
			h[src(i, j)] ++;

	showHistogram(nume, h, 256, 500);
	return h;
}

void testBinarizareCuPragAdaptiv() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);		
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);
		int iMin, iMax;
		int epsilon = 1;
		float T, TPrev, medieG1, medieG2;

		int h[256] = { 0 };
		//histograma
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				h[src(i, j)] ++;

		//i min, max
		for (int i = 0; i < 256; i++)
			if (h[i] > 0) {
				iMin = i;
				i = 256;
			}				
	
		for (int i = 255; i >= 0; i--)
			if (h[i] > 0) {
				iMax = i;
				i = 0;
			}
		
		T = (iMin + iMax) / 2;
		do {
			float sumaNumarator = 0;
			float sumaNumitor = 0;
			for (int i = iMin; i < T; i++) {
				sumaNumarator += (i * h[i]);
				sumaNumitor += h[i];
			}
			medieG1 = sumaNumarator / sumaNumitor;
			sumaNumarator = 0;
			sumaNumitor = 0;
			for (int i = T; i <= iMax; i++) {
				sumaNumarator += (i * h[i]);
				sumaNumitor += h[i];
			}
			medieG2 = sumaNumarator / sumaNumitor;
			TPrev = T;
			T = (medieG1 + medieG2) / 2;
		} while (abs(T - TPrev) > epsilon);

		std::cout << "\nprag" << T;
		
		//binarizare imagine
		for (int i = 0; i<height; i++)
		{
			for (int j = 0; j<width; j++)
			{
				if (src(i, j) < T)
					dst(i, j) = 0;
				else
					dst(i, j) = 255;
			}
		}
		imshow("binar", dst);
	}

}
void testModificareLuminozitate() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);
		int offset;

		std::cout << "Offset luminozitate\n";
		std::cin >> offset;

		for(int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {

				int pixel = src(i, j) + offset;
				if (pixel < 0)
					dst(i, j) = 0;
				else if (pixel > 255)
					dst(i, j) = 255;
				else
					dst(i, j) = pixel;
			}

		imshow("dst", dst);
		histograma(src, "src");
		histograma(dst, "dst");
	}
}
void testModificareContrast() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);
		int gOutMin, gOutMax;
		int gInMin, gInMax;
		int *hSrc;

		std::cout << "G out min\n";
		std::cin >> gOutMin;
		std::cout << "G out max\n";
		std::cin >> gOutMax;

		hSrc = histograma(src, "src");

		printVector(hSrc, 256);

		//i min, max
		for (int i = 0; i < 256; i++)
			if (hSrc[i] > 0) {
				gInMin = i;
				i = 256;
			}

		for (int i = 255; i >= 0; i--)
			if (hSrc[i] > 0) {
				gInMax = i;
				i = 0;
			}


		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {

				int pixel = gOutMin + (src(i, j) - gInMin) * (gOutMax - gOutMin) / (gInMax - gInMin);
				if (pixel < 0)
					dst(i, j) = 0;
				else if (pixel > 255)
					dst(i, j) = 255;
				else
					dst(i, j) = pixel;
			}

		imshow("dst__", dst);
		histograma(src, "src");
		histograma(dst, "dst");
	}
}
void testCorectiaGamma() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);
		float gama;
		std::cout << "gama\n";
		std::cin >> gama;

		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {
				int pixel = 255 * pow(src(i, j) / 256.0, gama);
				if (pixel < 0)
					dst(i, j) = 0;
				else if (pixel > 255)
					dst(i, j) = 255;
				else
					dst(i, j) = pixel;
			}

		imshow("dst__", dst);
	}
}
void testEgalizareaHistogramei() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);
		int *hSrc;
		float p[256], pc[256] = { 0 };
		float M = height * width;

		hSrc = histograma(src, "srcH");
		//printVector(hSrc, 256);
		
		//probabilitatea 
		for (int i = 0; i < 256; i++) {
			p[i] = hSrc[i] / M;
		}


		for (int i = 0; i < 256; i++)
				for (int k = 0; k <= i; k++) {
					pc[i] += p[k];
				}

		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {

				dst(i, j) = 255 * pc[src(i, j)];
			}

		//histograma(src, "src");
		histograma(dst, "dstH");

		imshow("dst__", dst);
	}
}

Mat_<uchar> convolutie(Mat_<uchar> src, int dim, int *nucleu, int normalizare) {
	Mat_<uchar> dst = Mat(src.rows, src.cols, CV_8UC1);

	for(int i = dim / 2; i < src.rows - dim / 2; i++)
		for (int j = dim / 2; j < src.cols - dim / 2; j++) {
			float suma = 0.0f;
			for(int k = -dim / 2; k <= dim / 2; k++)
				for (int m = -dim / 2; m <= dim / 2; m++)
					suma += src(i + k, j + m) * nucleu[(k + dim/2)*dim + (m + dim/2)];
			if(normalizare != 1)
				dst(i, j) = suma / normalizare;
			else {
				if (suma > 255)
					dst(i, j) = 255;
				else if (suma < 0)
					dst(i, j) = 0;
				else
					dst(i, j) = suma;
			}

		}
	return dst;
}

void testFiltruMedieAritmetica3x3() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);
		
		int dim = 3;
		int *nucleu = (int*)malloc(dim * dim * sizeof(int*));

		for(int i = 0; i < dim*dim ; i ++)
				nucleu[i] = 1;

		dst = convolutie(src, 3, nucleu, 9);
		imshow("src", src);
		imshow("filtru medie aritm", dst);
		waitKey();
	}
}

void testFiltruMedieAritmetica5x5() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);

		int dim = 5;
		int normalizare = 25;

		int *nucleu = (int*)malloc(dim * dim * sizeof(int*));
		
		for (int i = 0; i < dim*dim; i++)
			nucleu[i] = 1;

		dst = convolutie(src, dim, nucleu, normalizare);
		imshow("src", src);
		imshow("filtru medie aritm 5x5", dst);
		waitKey();
	}
}

void testFiltruGaussian() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);

		int dim = 3;
		int normalizare = 16;

		int vector[3][3] = { {1,2,1},{2,4,2},{1,2,1} };

		dst = convolutie(src, dim, vector[0], normalizare);
		imshow("src", src);
		imshow("filtru gaussian", dst);
		waitKey();
	}
}

void testFiltruLaplace() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);

		int dim = 3;
		int normalizare = 1;

		int vector[3][3] = { { 0,-1,0 },{ -1,4,-1},{ 0,-1,0} };

		dst = convolutie(src, dim, vector[0], normalizare);

		imshow("src", src);
		imshow("filtru laplace", dst);
		waitKey();
	}
}

void testFiltruTreceSus() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);

		int dim = 3;
		int normalizare = 1;

		int vector[3][3] = { { 0, -1, 0 },{ -1, 5, -1 },{ 0, -1, 0 } };

		dst = convolutie(src, dim, vector[0], normalizare);

		imshow("src", src);
		imshow("filtru laplace", dst);
		waitKey();
	}
}

void centering_transform(Mat img) {
	//expects floating point image
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {
			img.at<float>(i, j) = ((i + j) & 1) ? -img.at<float>(i, j) : img.at<float>(i, j);
		}
	}
}

Mat generic_freqency_domain_filter(Mat_<uchar> src, int option) {
	Mat_<float> srcf;
	src.convertTo(srcf, CV_32FC1);

	centering_transform(srcf);

	Mat fourier;
	dft(srcf, fourier, DFT_COMPLEX_OUTPUT);

	//split into real and imaginary channels
	Mat channels[] = { Mat::zeros(src.size(), CV_32F), Mat::zeros(src.size(), CV_32F) };
	split(fourier, channels);  // chanels[0] = Re(DFT(I), chanels[1] = Im(DFT(I))

							   //calculate magnitude and phase in floating point images mag and phi
	Mat mag, phi, dst;
	magnitude(channels[0], channels[1], mag);
	phase(channels[0], channels[1], phi);

	/*for (int i = 0; i < mag.rows; i++)
		for (int j = 0; j < mag.cols; j++)
			mag.at<float>(i, j) = log(1 + mag.at<float>(i, j));
		

	//normalize the result and put in the destination image
	normalize(mag, dst, 0, 255, NORM_MINMAX, CV_8UC1);*/

	// Insert filtering operations here ( chanles[0] = Re(DFT(I), chanels[1] = Im(DFT(I) )
	int poz;
	float coef;
	float R = 10; // filter "radius"
	int height = src.rows;
	int width = src.cols;

	int u = src.rows / 2;
	int v = src.cols / 2;

	switch (option) {
	case 1: break; // NO filter
	case 2:
		// FTJ ideal
		// inserati codul de filtrare aici ...
		for(int i = 0; i < src.rows; i++)
			for (int j = 0; j < src.cols; j++) {
				int poz = ((src.rows / 2 - i) * (src.rows / 2 - i) + (src.cols / 2 - j) * (src.cols / 2 - j));
				if (poz > R * R) {
					channels[0].at<float>(i, j) = 0;
					channels[1].at<float>(i, j) = 0;
				}
			}

		break;
	case 3:
		// FTS ideal
		// inserati codul de filtrare aici ...

		break;
	case 4:
		// FTJ gauss
		// inserati codul de filtrare aici ...

		break;
	case 5:
		// FTJ gauss
		// inserati codul de filtrare aici ...

		break;
	}

	//perform inverse transform and put results in dstf
	Mat dstf;
	merge(channels, 2, fourier);
	dft(fourier, dstf, DFT_INVERSE | DFT_REAL_OUTPUT | DFT_SCALE);
	centering_transform(dstf);
	//normalize the result and put in the destination image
	normalize(dstf, dst, 0, 255, NORM_MINMAX, CV_8UC1);
	//dstf.convertTo(dst, CV_8UC1);
	
	return dst;
}

void testLogMagnSpFourier() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);
		dst = generic_freqency_domain_filter(src, 1);
		imshow("src", src);
		imshow("dst", dst);
		waitKey();
	}
}

void testFTJIdeal() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);
		dst = generic_freqency_domain_filter(src, 2);
		imshow("src", src);
		imshow("dst", dst);
		waitKey();
	}
}

void testFTSIdeal() {

}

void testFTJTaiereGauss() {

}

void testFTSTaiereGauss() {

}

void testFiltruMedian() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);
		int w;
		printf("Dati w = ");
		scanf("%d", &w);

		for(int i = w/2; i < height - w/2; i++)
			for (int j = w / 2; j < width - w/2; j++) {
				std::vector<uchar> v;
				for (int k = i - w/2; k <= i + w/2; k++)
					for (int m = j - w/2; m <= j + w/2; m++) {
						v.push_back(src(k, m));
					}
				std::sort(v.begin(), v.end());
				dst(i, j) = v.at((w * w) / 2);
			}

		imshow("src", src);
		imshow("dst", dst);
	}
}

Mat_<uchar> convolutieFloat(Mat_<uchar> src, int dim, float *nucleu, float normalizare) {
	Mat_<uchar> dst = Mat(src.rows, src.cols, CV_8UC1);

	for (int i = dim / 2; i < src.rows - dim / 2; i++)
		for (int j = dim / 2; j < src.cols - dim / 2; j++) {
			float suma = 0.0f;
			for (int k = -dim / 2; k <= dim / 2; k++)
				for (int m = -dim / 2; m <= dim / 2; m++)
					suma += src(i + k, j + m) * nucleu[(k + dim / 2)*dim + (m + dim / 2)];
			dst(i, j) = suma / normalizare;
		}
	return dst;
}

void testFiltruGaussian1x2D() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst;
		float sigma;
		printf("Dati sigma = ");
		scanf("%f", &sigma);
		float wFloat;
		wFloat = 6 * sigma;
		int w = (int)wFloat;
		if (w % 2 == 0)
			w++;
		
		float *nucleu = (float*)malloc(w * w * sizeof(float*));

		for (int i = 0; i < w; i++)
			for (int j = 0; j < w; j++)
				nucleu[i * w + j] = (1 / (2 * PI* pow(sigma, 2))) * exp(-(pow((i - w/2), 2) + pow((j - w/2), 2)) / (2 * pow(sigma, 2)));

		float normalizare = 0;
		for (int i = 0; i < w * w; i++)
			normalizare += nucleu[i];
		std::cout << normalizare;

		double t = (double)getTickCount();
		dst = convolutieFloat(src, w, nucleu, normalizare);
		t = ((double)getTickCount() - t) / getTickFrequency();

		imshow("src", src);
		imshow("dst", dst);
		printf("Time = %.3f [ms]\n", t * 1000);
	}
}

void testFiltruGaussian2() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_8UC1);
		Mat_<uchar> dst2 = Mat(height, width, CV_8UC1);
		float sigma;
		printf("Dati sigma = ");
		scanf("%f", &sigma);
		float wFloat;
		wFloat = 6 * sigma;
		int w = (int)wFloat;
		if (w % 2 == 0)
			w++;

		float *nucleu = (float*)malloc(w * w * sizeof(float*));

		for (int i = 0; i < w; i++)
			for (int j = 0; j < w; j++)
				nucleu[i * w + j] = (1 / (2 * PI* pow(sigma, 2))) * exp(-(pow((i - w / 2), 2) + pow((j - w / 2), 2)) / (2 * pow(sigma, 2)));

		float *linie = (float*)malloc(w * sizeof(float*));
		float *coloana = (float*)malloc(w * sizeof(float*));
		float normalizareLinie = 0;
		float normalizareCol = 0;

		for (int i = 0; i < w; i++) {
			linie[i] = nucleu[w / 2 * w + i];
			normalizareLinie += linie[i];
		}			

		for (int i = 0; i < w; i++) {
			coloana[i] = nucleu[w / 2 + i * w];
			normalizareCol += coloana[i];
		}
		std::cout << normalizareLinie;
		std::cout << normalizareCol;

		double t = (double)getTickCount();

		for (int i = w / 2; i < height - w / 2; i++)
			for (int j = w / 2; j < width - w / 2; j++) {
				float suma = 0.0f;
				for (int k = -w / 2; k <= w / 2; k++)
						suma += src(i, j + k) * linie[k + w/2];
				dst(i, j) = suma / normalizareLinie;
			}

		for (int i = w / 2; i < height - w / 2; i++)
			for (int j = w / 2; j < width - w / 2; j++) {
				float suma = 0.0f;
				for (int k = -w / 2; k <= w / 2; k++)
					suma += dst(i + k, j) * coloana[k + w / 2];
				dst2(i, j) = suma / normalizareCol;
			}

		t = ((double)getTickCount() - t) / getTickFrequency();

		imshow("src", src);
		imshow("dst", dst2);
		printf("Time = %.3f [ms]\n", t * 1000);
	}
}


Mat_<int> convolutieInt(Mat_<uchar> src, int dim, int *nucleu, float normalizare) {
	Mat_<int> dst = Mat(src.rows, src.cols, CV_32SC1);

	for (int i = dim / 2; i < src.rows - dim / 2; i++)
		for (int j = dim / 2; j < src.cols - dim / 2; j++) {
			float suma = 0.0f;
			for (int k = -dim / 2; k <= dim / 2; k++)
				for (int m = -dim / 2; m <= dim / 2; m++)
					suma += src(i + k, j + m) * nucleu[(k + dim / 2)*dim + (m + dim / 2)];
			dst(i, j) = suma / normalizare;
		}
	return dst;
}

int getCadran(float fi) {	
	if ((fi >= -PI / 8 && fi <= PI / 8) || (fi <= -7 * PI / 8 || fi >= 7 * PI / 8))
		return 0;
	if ((fi >= PI / 8 && fi <= 3 * PI / 8) || (fi >= -7 * PI / 8 && fi <= -5 * PI / 8))
		return 1;
	if ((fi >= 3 * PI / 8 && fi <= 5 * PI / 8) || (fi >= -5 * PI / 8 && fi <= -3 * PI / 8))
		return 2;
	if ((fi >= 5 * PI / 8 && fi <= 7 * PI / 8) || (fi >= -3 * PI / 8 && fi <= -PI / 8))
		return 3;
	return -1;
}

void adaugaPuncteSlabeInQ(Mat_<uchar> src, int i, int j, std::queue<Point2i> &Q) {
	for (int k = i - 1; k <= i + 1; k++)
		for (int m = j - 1; m <= j + 1; m++)
			if (src(k, m) == 128)
			{
				Q.push(Point2i(k, m));
				src(k, m) = 255;
			}
}

void testDetectiaMuchiilorCanny() {
	char  fname[MAX_PATH];
	while (openFileDlg(fname)) {
		Mat_<uchar> src = imread(fname, CV_LOAD_IMAGE_GRAYSCALE);
		int height = src.rows;
		int width = src.cols;
		Mat_<uchar> dst = Mat(height, width, CV_32FC1);
		Mat_<int> Gx;
		Mat_<int> Gy;
		Mat_<int> GN = Mat(height, width, CV_32SC1);
		Mat_<int> GS = Mat(height, width, CV_32SC1);
		Mat_<float> f = Mat(height, width, CV_32FC1);
		Mat_<uchar> GShow = Mat(height, width, CV_8UC1);
		Mat_<uchar> GAfterTreshold = Mat(height, width, CV_8UC1);
		Mat_<uchar> GAfterPrelungire = Mat(height, width, CV_8UC1);

		int nucleuGx[9] = {-1, 0, 1, -2, 0, 2, -1, 0, 1};
		int nucleuGy[9] = {1, 2, 1, 0, 0, 0, -1, -2, -1};

		Gx = convolutieInt(src, 3, nucleuGx, 1);
		Gy = convolutieInt(src, 3, nucleuGy, 1);

		for(int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {
				GN(i, j) = sqrt(pow(Gx(i, j), 2) + pow(Gy(i, j), 2)) / (4*sqrt(2));
				f(i, j) = atan2(Gy(i, j), Gx(i, j));
			}

		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {
				GShow(i, j) = (uchar)GN(i, j);
			}

		imshow("GShow", GShow);

		for(int i = 1; i < height - 1; i++)
			for (int j = 1; j < width - 1; j++) {
				int cadran = getCadran(f(i, j));
				if (cadran == -1)
					return;
				switch (cadran)
				{
				case 0:
					if (GN(i, j) >= GN(i, j - 1) && GN(i, j) >= GN(i, j + 1))
						GS(i, j) = GN(i, j);
					else
						GS(i, j) = 0;
					break;
				case 1:
					if (GN(i, j) >= GN(i - 1, j + 1) && GN(i, j) >= GN(i + 1, j - 1))
						GS(i, j) = GN(i, j);
					else
						GS(i, j) = 0;
					break;
				case 2:
					if (GN(i, j) >= GN(i - 1, j) && GN(i, j)  >= GN(i + 1, j))
						GS(i, j) = GN(i, j);
					else
						GS(i, j) = 0;
					break;
				case 3:
					if (GN(i, j) >= GN(i - 1, j - 1) && GN(i, j) >= GN(i + 1, j + 1))
						GS(i, j) = GN(i, j);
					else
						GS(i, j) = 0;
					break;
				}
			}

		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++) {
				GShow(i, j) = (uchar)GS(i, j);
			}

		imshow("GShowFINAL", GShow);
		
		//histograma
		int h[256] = { 0 };
		for (int i = 1; i < height - 1; i++)
			for (int j = 1; j < width - 1; j++)
				h[GShow(i, j)] ++;

		float NrPctGradiwntNeNul = (height - 2) * (width - 2) - h[0];
		float p = 0.1;
		float NrPctMuchii = p * NrPctGradiwntNeNul;
		
		int Th = 255;
		for (int suma = 0; suma <= NrPctMuchii && Th >= 0; Th--)
			 suma += h[Th];
		
		std::cout << Th;
		
		float k = 0.4;
		float Tl = k * Th;

		for (int i = 1; i < height - 1; i++)
			for (int j = 1; j < width - 1; j++) {
				if (GShow(i, j) < Tl)
					GAfterTreshold(i, j) = 0;
				else if (GShow(i, j) < Th)
					GAfterTreshold(i, j) = 128;
				else
					GAfterTreshold(i, j) = 255;
			}

		imshow("GShowAfter Treshold", GAfterTreshold);

		//prelungirea muchiilor
		for (int i = 1; i < height - 1; i++)
			for (int j = 1; j < width - 1; j++)
				if (GAfterTreshold.at<uchar>(i, j) == 255) {
					std::queue<Point2i> Q;
					Q.push(Point2i(i, j));

					while (!Q.empty()) {
						Point2i q = Q.front();
						Q.pop();
						adaugaPuncteSlabeInQ(GAfterTreshold, q.x, q.y, Q);						
					}
				}

		for (int i = 1; i < height - 1; i++)
			for (int j = 1; j < width - 1; j++)
				if (GAfterTreshold.at<uchar>(i, j) == 128)
					GAfterTreshold(i, j) = 0;

		imshow("GAfterPrelungire", GAfterTreshold);
		waitKey();
	}
}


int main()
{
	int op;
	do
	{
		system("cls");
		destroyAllWindows();
		printf("Menu:\n");
		printf(" 1 - Open image\n");
		printf(" 2 - Open BMP images from folder\n");
		printf(" 3 - Image negative - diblook style\n");
		printf(" 4 - BGR->HSV\n");
		printf(" 5 - Resize image\n");
		printf(" 6 - Canny edge detection\n");
		printf(" 7 - Edges in a video sequence\n");
		printf(" 8 - Snap frame from live video\n");
		printf(" 9 - Mouse callback demo\n");
		printf(" 10 - Negative image\n");
		printf(" 11 - Change grey aditiv level\n");
		printf(" 12 - Change grey multiply level\n");
		printf(" 13 - ColorImageWhiteRedGreenYellow\n");
		printf(" 14 - InverseMatrix\n");
		printf(" 15 - copyRGBDifferentMatrixex\n");
		printf(" 16 - RGBtoGrayscale\n");
		printf(" 17 - Grayscale2WhiteBlack\n");
		printf(" 18 - RGBtoHSV\n");
		printf(" 19 - Labels\n");
		printf(" 20 - Labels2\n");
		printf(" 21 - Test contur \n");
		printf(" 22 - Test contur fisier \n");
		printf(" 23 - Test dilatare \n");
		printf(" 24 - Test eroziune \n");
		printf(" 25 - Test deschidere \n");
		printf(" 26 - Test inchidere \n");
		printf(" 27 - Test extragere contur \n");
		printf(" 28 - Test umplerea regiunilor \n");
		printf(" 29 - Test histograma, media, deviatia standard \n");
		printf(" 30 - Test binarizare cu prag adaptiv \n");
		printf(" 31 - Modificare luminozitate \n");
		printf(" 32 - Modificare constrast \n");
		printf(" 33 - Corectia Gamma \n");
		printf(" 34 - Egalizarea histogramei \n");
		printf(" 35 - Filtru medie aritmeica 3x3\n");
		printf(" 36 - Filtru medie aritmetica 5x5\n");
		printf(" 37 - Filtru gaussian\n");
		printf(" 38 - Filtru Laplace\n");
		printf(" 39 - Filtru trece sus\n");
		printf(" 40 - Log magn sp Fourier\n");
		printf(" 41 - FTJ ideal\n");
		printf(" 42 - FTS ideal\n");
		printf(" 43 - FTJ taiere Gauss\n");
		printf(" 44 - FTS taiere Gauss\n");
		printf(" 45 - Filtru median\n");
		printf(" 46 - Filtru gaussian 1x2D\n");
		printf(" 47 - Filtru gaussian 2\n");
		printf(" 48 - DetectiaMuchiilorCanny\n");
		printf(" 0 - Exit\n\n");
		printf("Option: ");
		scanf("%d",&op);
		switch (op)
		{
			case 1:
				testOpenImage();
				break;
			case 2:
				testOpenImagesFld();
				break;
			case 3:
				testParcurgereSimplaDiblookStyle(); //diblook style
				break;
			case 4:
				//testColor2Gray();
				testBGR2HSV();
				break;
			case 5:
				testResize();
				break;
			case 6:
				testCanny();
				break;
			case 7:
				testVideoSequence();
				break;
			case 8:
				testSnap();
				break;
			case 9:
				testMouseClick();
				break;
			case 10:
				testNegativeImage();
				break;
			case 11:
				testChangeGreyLevelAditivFactor();
				break;
			case 12:
				testChangeGreyLevelMultiplyFactor();
				break;
			case 13:
				testColorImageWhiteRedGreenYellow();
				break;
			case 14:
				testInverseMatrix();
				break;
			case 15:
				copyRGBDifferentMatrixes();
				break;
			case 16:
				RGBtoGrayscale();
				break;
			case 17:
				Grayscale2WhiteBlack();
				break;
			case 18:
				RGBtoHSV();
				break;
			case 19:
				testMakeLabels();
				break;
			case 20:
				testMakeLabels2();
				break;
			case 21:
				testContur();
				break;
			case 22:
				testConturFisier();
				break;
			case 23:
				testDilatare();
				break;
			case 24:
				testEroziune();
				break;
			case 25:
				testDeschidere();
				break;
			case 26:
				testInchidere();
				break;
			case 27:
				testExtragereContur();
				break;
			case 28:
				testUmplereRegiune();
				break;
			case 29:
				testHistograma();
				break;
			case 30:
				testBinarizareCuPragAdaptiv();
				break;
			case 31:
				testModificareLuminozitate();
				break;
			case 32:
				testModificareContrast();
				break;
			case 33:
				testCorectiaGamma();
				break;
			case 34:
				testEgalizareaHistogramei();
				break;
			case 35:
				testFiltruMedieAritmetica3x3();
				break;
			case 36:
				testFiltruMedieAritmetica5x5();
				break;
			case 37:
				testFiltruGaussian(); 
				break;
			case 38:
				testFiltruLaplace(); 
				break;
			case 39:
				testFiltruTreceSus(); 
				break;
			case 40:
				testLogMagnSpFourier(); 
				break;
			case 41:
				testFTJIdeal();
				break;
			case 42:
				testFTSIdeal(); 
				break;
			case 43:
				testFTJTaiereGauss(); 
				break;
			case 44:
				testFTSTaiereGauss();
				break;
			case 45:
				testFiltruMedian(); 
				break;
			case 46:
				testFiltruGaussian1x2D(); 
				break;
			case 47:
				testFiltruGaussian2(); 
				break;
			case 48:
				testDetectiaMuchiilorCanny();
				break;
		}
	}
	while (op!=0);
	return 0;
}